#include <cmath>
#include <cstdlib>
#include <omp.h>
#include <ctime>
#include <cstring>
#include <iostream>

#define IDX(i,j) (size*(i) + (j))

/** 
 * Decomposes a matrix in-situ into its L and U components
 * @param matrix pointer to the matrix
 * @param size   size of the matrix
 */
void decompose_matrix(double *matrix, int size)
{
    /* in situ LU decomposition */
    int k;

    //LU-decomposition based on Gaussian Elimination
    // - Arranged so that the multiplier doesn't have to be computed multiple times
    for(k = 0; k < size-1; k++){ //iterate over rows/columns for elimination
        int row;
        // The "multiplier" is the factor by which a row is multiplied when
        //  being subtracted from another row.
#pragma omp parallel for private(row) shared(matrix)
        for(row = k + 1; row < size; row++){
            int col;

            // the multiplier only depends on (k,row),
            // it is invariant with respect to col
            double factor = matrix[IDX(row,k)]/matrix[IDX(k,k)];

            //Eliminate entries in sub matrix (subtract rows)
            for(col = k + 1; col < size; col++){ //column
                matrix[IDX(row,col)] = matrix[IDX(row,col)] - factor*matrix[IDX(k,col)];
            }

            matrix[IDX(row,k)] = factor;
        }
    }
}

#define EPSILON (1.0E-3)

inline int min(int a, int b){
    if(a < b)
        return a;
    else
        return b;
}

/** 
 * Checks if the computation of l*u is the same as matrix
 * @param lu     pointer to the lu matrix
 * @param matrix pointer to the matrix
 * @param size   size of the matrix
 * @return       true if l*u=matrix, false otherwise
 */
int check_matrix(double *lu, double *matrix, int size)
{
    /* return l*u == matrix */

    int row;
    double s = 0.0;

    //Compute l*u element-wise and compare those elements to the original matrix.
    //The deviation of all elements is aggregated in `s`. The matrices are equal
    //  when this aggregated deviation is zero (or close to it)
    //The multiplication itself is a naïve matrix multiplication, modified to
    //  work with the embedding of L and U in `lu`.
#pragma omp parallel for private(row) reduction(+: s)
    for(row = 0; row < size; row++){ // row of matrix
        int col, k;
        for(col = 0; col < size; col++){ // column of matrix
            double r = 0.0;
            int limit = min(row-1,col);
            for(k = 0; k <= limit; k++){ // index in sum
                double l = lu[IDX(row,k)]; // has 1's on the diagonal
                double u = lu[IDX(k,col)];
                r += l*u;
            }
            //The diagonal of L is treated separately to keep the previous loop simple
            if(col >= row){
                double l = 1.0;
                double u = lu[IDX(row,col)];
                r += l*u;
            }

            //Check whether there is a difference between L*U and the original matrix.
            r = r - matrix[IDX(row,col)];
            r = fabs(r);
            if(r < EPSILON)
                r = 0.0;

            s += r;
        }
    }

    return s < EPSILON;
}

void gen_matrix(double* matrix, int size)
{
    srand(std::time(NULL));
    for (int i = 0; i < size * size; i++) {
        matrix[i] = -10.0 + (double)rand() / RAND_MAX * 20.0;
    }
}

int main(int argc, char const* argv[])
{
    clock_t start_time;
    clock_t end_time;

    int size = 1 << 10;
    double* matrix = (double*)malloc(size * size * sizeof(double));
    double* lu = (double*)malloc(size * size * sizeof(double));

    start_time = clock();
    gen_matrix(matrix, size);
    end_time = clock();
    std::cout << "gen_matrix in " << ((double)end_time - start_time) / CLOCKS_PER_SEC << std::endl;

    start_time = clock();
    memcpy(lu, matrix, size * size * sizeof(double));
    end_time = clock();
    std::cout << "memcpy in " << ((double)end_time - start_time) / CLOCKS_PER_SEC << std::endl;

    // print_matrix(matrix, size);

    start_time = clock();
    decompose_matrix(lu, size);
    end_time = clock();
    std::cout << "lu_factorization in " << ((double)end_time - start_time) / CLOCKS_PER_SEC
              << std::endl;

    start_time = clock();
    double diff = check_matrix(lu, matrix, size);
    end_time = clock();
    std::cout << "count_diff in " << ((double)end_time - start_time) / CLOCKS_PER_SEC << std::endl;

    // print_lu(lu, size);
    std::cout << "diff: " << diff << std::endl;
    return 0;
}