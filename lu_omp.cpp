#include <cmath>
#include <cstdlib>
#include <omp.h>
#include <ctime>
#include <cstring>
#include <iostream>

#define EPS (1.0E-6)

void lu_factorization(double** matrix, int size)
{
    for (int k = 0; k < size - 1; k++) {
        for (int i = k + 1; i < size; i++) {
            matrix[i][k] /= matrix[k][k];
        }

        int i, j;
        omp_set_dynamic(0);
#pragma omp parallel for shared(matrix, k, size) private(i, j) num_threads(4)
        for (i = k + 1; i < size; i++) {
            for (j = k + 1; j < size; j++) {
                matrix[i][j] -= matrix[i][k] * matrix[k][j];
            }
        }
    }
}


double count_diff(double** lu, double** matrix, int size)
{
    int row;
    double s = 0;
#pragma omp parallel private(row) reduction(+ : s)
#pragma omp for
    for (row = 0; row < size; row++) {
        int col, k;
        for (col = 0; col < size; col++) {
            double r = 0;
            int lim = fmin(row - 1, col);
            for (k = 0; k <= lim; k++) {
                double l = lu[row][k];
                double u = lu[k][col];
                r += l * u;
            }

            if (col >= row) {
                double l = 1.0;
                double u = lu[row][col];
                r += l * u;
            }

            r = r - matrix[row][col];
            r = fabs(r);
            if (r < EPS) {
                r = 0.0;
            }

            s += r;
        }
    }

    return s;
}

void print_matrix(double** matrix, int size)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void print_lu(double** lu, int size)
{
    std::cout << "L:" << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < i; j++) {
            std::cout << lu[i][j] << " ";
        }
        std::cout << 1 << std::endl;
    }

    std::cout << "U:" << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
            std::cout << lu[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void gen_matrix(double** matrix, int size)
{
    srand(std::time(NULL));
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrix[i][j] = -10.0 + (double)rand() / RAND_MAX * 20.0;
        }
    }
}

int main(int argc, char const* argv[])
{
    double start_time;
    double end_time;

    int size = 1 << 11;

    double** matrix = new double*[size];
    for(int i = 0; i < size; ++i)
        matrix[i] = new double[size];

    double** lu = new double*[size];
    for(int i = 0; i < size; ++i)
        lu[i] = new double[size];

    start_time = omp_get_wtime();
    gen_matrix(matrix, size);
    end_time = omp_get_wtime();
    std::cout << "gen_matrix in " << end_time - start_time << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            lu[i][j] = matrix[i][j];
        }
    }
    start_time = omp_get_wtime();
    
    end_time = omp_get_wtime();
    std::cout << "memcpy in " << end_time - start_time << std::endl;

    // print_matrix(matrix, size);

    start_time = omp_get_wtime();
    lu_factorization(lu, size);
    end_time = omp_get_wtime();
    std::cout << "lu_factorization in " << end_time - start_time
              << std::endl;

    start_time = omp_get_wtime();
    double diff = count_diff(lu, matrix, size);
    end_time = omp_get_wtime();
    std::cout << "count_diff in " << end_time - start_time << std::endl;

    // print_lu(lu, size);
    std::cout << "diff: " << diff << std::endl;
    return 0;
}