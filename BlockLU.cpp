#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <iostream>
#include <omp.h>

const int 		MATRIX_SIZE = 1 << 10;
const int 		BLOCK_SIZE 	= 8;
const double 	EPS 		= 0.0000001;

#define BLOCKS_NUMBER MATRIX_SIZE / BLOCK_SIZE


void initializeMatrix (double** matrix)
{
	for (int i = 0; i < MATRIX_SIZE; i++) {
		for (int j = 0; j < MATRIX_SIZE; j++) {
			matrix[i][j] = -1.0 + (double)rand() / RAND_MAX * 2.0;
		}
	}
}

void printMatrix(double** matrix)
{
	std::cout << "Matrix:" << std::endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void copyMatrix (double** from, double** to)
{
	for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            to[i][j] = from[i][j];
        }
    }
}

void printLU(double** lu)
{
    std::cout << "L:" << std::endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < i; j++) {
            std::cout << lu[i][j] << " ";
        }
        std::cout << 1 << std::endl;
    }

    std::cout << "U:" << std::endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = i; j < MATRIX_SIZE; j++) {
            std::cout << lu[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

double countDifference(double** lu, double** matrix)
{
    int row;
    double s = 0;
    for (row = 0; row < MATRIX_SIZE; row++) {
        int col, k;
        for (col = 0; col < MATRIX_SIZE; col++) {
            double r = 0;
            int lim = fmin(row - 1, col);
            for (k = 0; k <= lim; k++) {
                double l = lu[row][k];
                double u = lu[k][col];
                r += l * u;
            }

            if (col >= row) {
                double l = 1.0;
                double u = lu[row][col];
                r += l * u;
            }

            r = r - matrix[row][col];
            r = fabs(r);
            if (r < EPS) {
                r = 0.0;
            }

            s += r;
        }
    }

    return s;
}

void decomposeBlock (double** matrix, int blockNumber)
{
	int indent = blockNumber * BLOCK_SIZE;

	for (int k = indent; k < indent + BLOCK_SIZE - 1; k++) {
		for (int i = k + 1; i < indent + BLOCK_SIZE; i++) {
			matrix[i][k] /= matrix[k][k];
		}

		
			for (int i = k + 1; i < indent + BLOCK_SIZE; i++) {
            	for (int j = k + 1; j < indent + BLOCK_SIZE; j++) {
                	matrix[i][j] -= matrix[i][k] * matrix[k][j];
            	}
        	}
		
        
	}

	//Считаем верхний правый U блок (лежачая матрица)
	
	for (int k = indent; k < indent + BLOCK_SIZE; k++) {
		#pragma omp parallel
		{
			#pragma omp for
			for (int l = indent + BLOCK_SIZE; l < MATRIX_SIZE; l++) {
				for (int i = indent; i < k; i++) {
					matrix[k][l] -= matrix[k][i] * matrix[i][l];
				}
			}
		}
	}
		
	

	//Считаем нижний левый L блок (стоячая матрица)
	
		
	for (int l = indent; l < indent + BLOCK_SIZE; l++) {
		#pragma omp parallel
		{
			#pragma omp for
			for (int k = indent + BLOCK_SIZE; k < MATRIX_SIZE; k++) {
				for (int i = indent; i < l; i++) {
					matrix[k][l] -= matrix[k][i] * matrix[i][l];
				}
				matrix[k][l] /= matrix[l][l];
			}
		}
		
	}
	
	

	// Считаем правый нижний блок A
	// Блочно перемножаем L на U
	#pragma omp parallel
	{
		#pragma omp for
		for (int leftBlock = 0; leftBlock < BLOCKS_NUMBER - blockNumber - 1; leftBlock++) {
			for (int rightBlock = 0; rightBlock < BLOCKS_NUMBER - blockNumber - 1; rightBlock++) {
				for (int k = indent + BLOCK_SIZE * (leftBlock + 1); k < indent + BLOCK_SIZE * (leftBlock + 2); k++) {
					for (int l = indent + BLOCK_SIZE * (rightBlock + 1); l < indent + BLOCK_SIZE * (rightBlock + 2); l++) {
						double sum = 0.0;
						for (int i = indent; i < indent + BLOCK_SIZE; i++) {
							sum += matrix[k][i] * matrix[i][l];
						}
						matrix[k][l] -= sum;
					}
				}
							
			}
		}
	}
			
}

void decomposeBlockNoOmp (double** matrix, int blockNumber)
{
	int indent = blockNumber * BLOCK_SIZE;

	for (int k = indent; k < indent + BLOCK_SIZE - 1; k++) {
		for (int i = k + 1; i < indent + BLOCK_SIZE; i++) {
			matrix[i][k] /= matrix[k][k];
		}

		
			for (int i = k + 1; i < indent + BLOCK_SIZE; i++) {
            	for (int j = k + 1; j < indent + BLOCK_SIZE; j++) {
                	matrix[i][j] -= matrix[i][k] * matrix[k][j];
            	}
        	}
		
        
	}

	//Считаем верхний правый U блок (лежачая матрица)
	for (int k = indent; k < indent + BLOCK_SIZE; k++) {
		
		for (int l = indent + BLOCK_SIZE; l < MATRIX_SIZE; l++) {
			for (int i = indent; i < k; i++) {
				matrix[k][l] -= matrix[k][i] * matrix[i][l];
			}
		}
		
		
	}

	//Считаем нижний левый L блок (стоячая матрица)
	for (int l = indent; l < indent + BLOCK_SIZE; l++) {
		
			for (int k = indent + BLOCK_SIZE; k < MATRIX_SIZE; k++) {
				for (int i = indent; i < l; i++) {
					matrix[k][l] -= matrix[k][i] * matrix[i][l];
				}
				matrix[k][l] /= matrix[l][l];
			}
		
	}

	//Считаем правый нижний блок A
	//Блочно перемножаем L на U
	for (int leftBlock = 0; leftBlock < BLOCKS_NUMBER - blockNumber - 1; leftBlock++) {
		for (int rightBlock = 0; rightBlock < BLOCKS_NUMBER - blockNumber - 1; rightBlock++) {
			for (int k = indent + BLOCK_SIZE * (leftBlock + 1); k < indent + BLOCK_SIZE * (leftBlock + 2); k++) {
				for (int l = indent + BLOCK_SIZE * (rightBlock + 1); l < indent + BLOCK_SIZE * (rightBlock + 2); l++) {
					for (int i = indent; i < indent + BLOCK_SIZE; i++) {
						matrix[k][l] -= matrix[k][i] * matrix[i][l];
					}
				}
			}			
		}
	}
}

int main(int argc, char const *argv[])
{
	double** matrix = new double*[MATRIX_SIZE];
    for(int i = 0; i < MATRIX_SIZE; i++) {
        matrix[i] = new double[MATRIX_SIZE];
    }
    initializeMatrix(matrix);

	double** matrixCopy = new double*[MATRIX_SIZE];
    for(int i = 0; i < MATRIX_SIZE; i++) {
        matrixCopy[i] = new double[MATRIX_SIZE];
    }

    double copyMatrixTime = -omp_get_wtime();
    copyMatrix(matrix, matrixCopy);
    copyMatrixTime += omp_get_wtime();
    std::cout << "copyMatrixTime = " << copyMatrixTime << std::endl;

	// printMatrix(matrix);
	// std::cout << std::endl;

   	double decompositionTime = -omp_get_wtime();
    std::cout << std::endl;
	for (int blockNumber = 0; blockNumber < BLOCKS_NUMBER; blockNumber++) {
		double decomposeBlockTime = -omp_get_wtime();
		decomposeBlock(matrix, blockNumber);
		decomposeBlockTime += omp_get_wtime();

		std::cout << "Block #" << blockNumber << " decomposed in " << decomposeBlockTime << std::endl;
	}
	decompositionTime += omp_get_wtime();
	std::cout << std::endl << "Decomposition finished in " << decompositionTime << std::endl;

	// copyMatrix(matrixCopy, matrix);

 //    double noOmpTime = -omp_get_wtime();
	// for (int blockNumber = 0; blockNumber < BLOCKS_NUMBER; blockNumber++) {
	// 	double decomposeBlockTime = -omp_get_wtime();
	// 	decomposeBlockNoOmp(matrix, blockNumber);
	// 	decomposeBlockTime += omp_get_wtime();

	// 	std::cout << "Block #" << blockNumber << " decomposed without omp in " << decomposeBlockTime << std::endl;
	// }
	// noOmpTime += omp_get_wtime();
	// std::cout << std::endl << "Decomposition without omp finished in " << noOmpTime << std::endl;

	// printLU(matrix);
	
	std::cout << std::endl << "Difference: " << countDifference(matrix, matrixCopy) << std::endl;

	return 0;
}